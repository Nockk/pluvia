using System.Collections;
using System.Collections.Generic;
using Episodes;
using UnityEngine;
using UnityEngine.UI;
using Player;
using UI.ParticleSystem;

public class ToothPasteManager : MonoBehaviour
{
    private Vector2 _mousePos;
    private PlayerPActions _playerInputs;
    [SerializeField] private GameObject hand;
    [SerializeField] private GameObject mg;
    [SerializeField] private EpisodeTask episodeTask;
    [SerializeField] private ParticleManager particleManager;
    public bool isDone = false;
    
    // Start is called before the first frame update
    void Start()
    {
        //Set Cursor to not be visible
        Cursor.visible = false;

        _playerInputs = PlayerInputs.PlayerControls;
        _playerInputs.Player.Enable();
        _playerInputs.UI.Enable();
    }

    // Update is called once per frame
    void Update()
    {
        _mousePos = _playerInputs.UI.Point.ReadValue<Vector2>();
        hand.transform.position = new Vector3(_mousePos.x, _mousePos.y, 0);
        particleManager.SetSpawnPosition(hand.transform.position);
    }

    void OnTriggerStay2D(Collider2D other) 
    {
        Debug.Log ("Triggered");
        Color c = this.GetComponent<Image>().color;
        c.a -= 0.001f;
        this.GetComponent<Image>().color = c;
        particleManager.StartSpawning();
        if (c.a <= 0.0f && !isDone){
            isDone = true;
            StartCoroutine(SetTaskCompleted());
            
        }
    }

    private IEnumerator SetTaskCompleted()
    {
        yield return new WaitForSeconds(1f);
        episodeTask.SetCompleted();
        yield return null;
    }

    public void ActivateMG(){
        mg.SetActive(true);
    }

    public void DeactivateMG(){
        mg.SetActive(false);
    }

}
