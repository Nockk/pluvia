﻿using System;
using TMPro;
using UnityEngine;

namespace Episodes
{
    public class Episode : MonoBehaviour
    {
        [SerializeField] 
        private string name;
        [SerializeField]
        private EpisodeTask[] tasks;

        [SerializeField] private TextMeshProUGUI taskTitle, taskDescription;
        
        private int currentTaskIndex = 0;
        
        public Action OnAllTasksCompleted;
        
        private void Start()
        {
            EpisodeTask task = GetTask();
            PrintTask(task.Name, task.Description);
            Debug.Log(task.Name);

            task.OnTaskCompleted += OnCurrentTaskCompleted;
        }

        private void Update()
        {
            PrintTasks();
        }

        private EpisodeTask GetTask()
        {
            for (; currentTaskIndex < tasks.Length; currentTaskIndex++)
            {
                EpisodeTask episodeTask = tasks[currentTaskIndex];
                if (!episodeTask.IsCompleted)
                {
                    return episodeTask;
                }
            }

            return null;
        }

        private void OnCurrentTaskCompleted()
        {
            EpisodeTask task = GetTask();
            if (task == null)
            {
                OnAllTasksCompleted.Invoke();
                return;
            }
            Debug.Log(task.Name);
            PrintTask(task.Name, task.Description);
            task.OnTaskCompleted += OnCurrentTaskCompleted;
        }

        private void PrintTasks()
        {
            string output = String.Empty;
            foreach (EpisodeTask task in tasks)
            {
                output += $"{task.Name} -> {task.IsCompleted}\n";
            }
            Debug.Log($"Tasks: {output}");
        }

        private void PrintTask(string title, string description)
        {
            taskTitle.SetText(title);
            taskDescription.SetText(description);
        }
    }
}