﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Episodes
{
    public class EpisodeTask : MonoBehaviour
    {
        public string Name;
        public string Description;
        public bool IsCompleted;

        public Action OnTaskCompleted;

        public void SetCompleted()
        {
            IsCompleted = true;
            OnTaskCompleted?.Invoke();
        }
    }
}