﻿using System;
using System.Collections;
using System.Collections.Generic;
using Episodes;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;

public class GameDirector : Singleton<GameDirector>
{
    [Header("Systems")] 
    [SerializeField] private GameObject world;
    [SerializeField] private GameObject systems;
    [SerializeField] private GameObject gui;
    [SerializeField] private GameObject cameras;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject blackScreen;
    [SerializeField] private GameObject endGameScreen;
    private Animation _animationFadToBlack;
    
    [SerializeField] 
    private Episode[] episodes;

    private int currentEpisodeIndex = 0;
    private Episode currentEpisode;
    
    public int GetCurrentEpisodeIndex => currentEpisodeIndex;

    public void EnableSystems()
    {
        
        world.SetActive(true);
        systems.SetActive(true);
        gui.SetActive(true);
        cameras.SetActive(true);
        player.SetActive(true);
        
        StartEpisode();
    }

    public void EnableSystems(bool value)
    {
        world.SetActive(value);
        systems.SetActive(value);
        gui.SetActive(value);
        cameras.SetActive(value);
        player.SetActive(value);
        
        StartEpisode();
    }
    

    private void StartEpisode()
    {
        currentEpisode = GetEpisode();
    }
    
    private Episode GetEpisode()
    {
        for (; currentEpisodeIndex < episodes.Length; currentEpisodeIndex++)
        {
            Episode episode = episodes[currentEpisodeIndex];
            if (episode == null)
            {
                return null;
            } 
            episode.OnAllTasksCompleted += LoadNextEpisode;
        }

        return null;
    }
    
    private void LoadNextEpisode()
    {
        
    }
    
    private IEnumerator PlayFadeCoroutine(GameObject currentScene)
    {
        _animationFadToBlack.Play();
        
        yield return new WaitUntil(() => !_animationFadToBlack.isPlaying);

        blackScreen.SetActive(false);
        currentScene.SetActive(false);
        EnableSystems();
        yield return null;

    }
    
    private IEnumerator PlayFadeCoroutine()
    {
        _animationFadToBlack.Play();
        
        yield return new WaitUntil(() => !_animationFadToBlack.isPlaying);

        blackScreen.SetActive(false);
        yield return null;

    }
    
    private IEnumerator PlayFadeCoroutine(GameObject nextScene, bool enable)
    {
        _animationFadToBlack.Play();
        
        yield return new WaitUntil(() => !_animationFadToBlack.isPlaying);

        blackScreen.SetActive(false);
        EnableSystems(false);
        nextScene.SetActive(enable);
        yield return null;

    }

    public void ChangeScene(GameObject currentScene)
    {
        blackScreen.SetActive(true);
        if (_animationFadToBlack == null)
            _animationFadToBlack = blackScreen.GetComponent<Animation>();

        StartCoroutine(PlayFadeCoroutine(currentScene));
    }
    
    public void BlackScreenEndGame()
    {
        blackScreen.SetActive(true);
        if (_animationFadToBlack == null)
            _animationFadToBlack = blackScreen.GetComponent<Animation>();

        StartCoroutine(PlayFadeCoroutine());
        
    }
    
    public void ChangeToScene(GameObject nextScene)
    {
        blackScreen.SetActive(true);
        if (_animationFadToBlack == null)
            _animationFadToBlack = blackScreen.GetComponent<Animation>();

        StartCoroutine(PlayFadeCoroutine(nextScene, true));
        
    }

    public void EndGame()
    {
        ChangeToScene(endGameScreen);
        StartCoroutine(GoToMenuCoroutine());
    }

    private IEnumerator GoToMenuCoroutine()
    {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadSceneAsync(0);
        yield return null;
    }
}