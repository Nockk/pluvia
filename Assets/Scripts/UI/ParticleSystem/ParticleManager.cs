﻿using System.Collections;
using UnityEngine;

namespace UI.ParticleSystem
{
    public class ParticleManager : MonoBehaviour
    {
        [SerializeField] private GameObject spritePrefab;
        [SerializeField] private Vector3 spawnOffset = new Vector3(-40f, -53f, 0.0f);

        [SerializeField] private float spawnRate = 0.2f;

        private Vector3 spawnPosition;
        private bool isSpawning;

        public void StartSpawning()
        {
            if (!isSpawning)
                StartCoroutine(SpawnParticleCoroutine());
        }

        private IEnumerator SpawnParticleCoroutine()
        {
            isSpawning = true;
            for (int i = 0; i < 10; i++)
            {
                GameObject instantiatedObject = Instantiate(spritePrefab, spawnPosition + spawnOffset, Quaternion.identity); 
                
                instantiatedObject.transform.SetParent(transform.parent);
                yield return new WaitForSeconds(spawnRate);
            }

            isSpawning = false;
            yield return null;
        }

        public void SetSpawnPosition(Vector3 position) => spawnPosition = position;
    }
}