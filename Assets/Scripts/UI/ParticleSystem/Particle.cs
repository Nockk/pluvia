﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UI.ParticleSystem
{
    public class Particle : MonoBehaviour
    {
        [SerializeField] 
        private float speed = -40f;
        [SerializeField] 
        private float timeAlive = 0.5f;
        [SerializeField] 
        private bool collision = false;
        [SerializeField] 
        private bool spawnRandomDirection = false;

        private Vector3 randomDirection;

        private void Awake()
        {
            Destroy(gameObject, timeAlive);
        }

        private void Start()
        {
            randomDirection = Random.insideUnitCircle;
        }

        private void Update()
        {
            Vector3 position = transform.position;
            if (spawnRandomDirection)
            {
                position += randomDirection * Time.deltaTime;
            }
            else
            {
                position.y += speed * Time.deltaTime;
            }
            
            transform.position = position;
        }
    }
}