﻿using System;
using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;
using UnityEngine.InputSystem;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private GameObject _pauseMenu;
    [SerializeField] private GameObject _pauseMenuPanel;

    private PlayerPActions _controls;

    private void OnEnable()
    {
        
    }

    private void Awake()
    {
        _controls = PlayerInputs.PlayerControls;
        _controls.Player.PauseMenu.Enable();
        _controls.Player.PauseMenu.performed += PauseMenuOnperformed;
        
    }

    private void PauseMenuOnperformed(InputAction.CallbackContext obj)
    {
        PauseMenuPanel();
    }

    private void PauseMenuPanel()
    {
        _pauseMenu.SetActive(!_pauseMenu.activeSelf);
        bool isPauseMenuActive = _pauseMenu.activeSelf;
        
        _pauseMenuPanel.SetActive(isPauseMenuActive);
        
        if (isPauseMenuActive)
        {
            _controls.Player.Move.Disable();
            _controls.UI.Enable();
            _controls.UI.Navigate.Enable();
            _controls.UI.Submit.Enable();
        }
        else
        {
            _controls.UI.Disable();
            _controls.Player.Move.Enable();
        }
        
       
    }

    public void ContinueBtn()
    {
        _pauseMenuPanel.SetActive(false);
        _pauseMenu.SetActive(false);
        
        _controls.UI.Disable();
        _controls.Player.Move.Enable();
    }
    
    public void QuitBtn()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }
    
}
