﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MiniGames
{
    public class SNSheep : MonoBehaviour
    {
        [Header("Movement")] 
        [SerializeField]
        private float movementSpeed = 0.9f;

        [SerializeField, Min(0.0f)] 
        private float maxSpeed = 5.0f;

        [SerializeField] private float speedTimeMultiplier = 0.1f;

        [SerializeField] 
        private Vector3 maxDistanceOffset = Vector3.one;
        [SerializeField] 
        private Vector3 minDistanceOffset = Vector3.one;

        private Vector3 distanceOffset;
        
        [Header("Animation")]
        [SerializeField, Min(0.0f)] 
        private float timeToDark = 3.0f;
        [SerializeField, Min(0.0f)]
        private float splitTime = 0.001f;
        
        [SerializeField] 
        private float darkValue;

        [SerializeField] 
        private Sprite badShip;

        [SerializeField] 
        private string PlayerTag = "PlayerSheep";
        
        private SpriteRenderer _spriteRenderer;
        private bool _animationEnded;
        
        private Transform playerTransform;

        private void Start()
        {
            playerTransform = GameObject.FindWithTag(PlayerTag).transform;
            _spriteRenderer = GetComponent<SpriteRenderer>();
            RandomDistOffset();
            StartCoroutine(AnimateSheep());
        }

        private IEnumerator AnimateSheep()
        {
            for (float currentDark = 0.0f; currentDark <= darkValue; currentDark += splitTime)
            {
                yield return new WaitForSeconds(splitTime);
                Color spriteColor = _spriteRenderer.color;

                _spriteRenderer.color = new Color(spriteColor.r - splitTime, spriteColor.g - splitTime, spriteColor.b - splitTime);
            }
            
            _spriteRenderer.sprite = badShip;
            _animationEnded = true;
        }

        private void Update()
        {
            FollowPlayer();
        }

        private void FollowPlayer()
        {
            Vector3 distance = (playerTransform.position - transform.position) - distanceOffset;
            movementSpeed += speedTimeMultiplier;
            movementSpeed = Mathf.Clamp(movementSpeed, 0.0f, maxSpeed);
            Vector3 newPosition = distance.normalized * movementSpeed * Time.deltaTime;
            //transform.localPosition = Vector3.Lerp(transform.localPosition, transform.localPosition + newPosition, 0.9f);
            transform.localPosition += newPosition;
        }

        private void RandomDistOffset()
        {
            float x = Random.Range(minDistanceOffset.x, maxDistanceOffset.x);
            float y = Random.Range(minDistanceOffset.y, maxDistanceOffset.y);
            float z = Random.Range(minDistanceOffset.z, maxDistanceOffset.z);
            
            distanceOffset = new Vector3(x, y , z);
        }

    }
}