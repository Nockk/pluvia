﻿using System;
using UnityEngine;

namespace MiniGames
{
    public class SNInfiniteScroll : MonoBehaviour
    {
        private float _textureUnitSizeX;
        [SerializeField]
        private Transform cameraTransform;

        [SerializeField] private Vector2 parallaxEffectMultiplier;

        private Vector3 _lastCameraPosition;
        
        private void Start()
        {
            _lastCameraPosition = cameraTransform.localPosition;
            Sprite sprite = GetComponent<SpriteRenderer>().sprite;
            Texture2D texture = sprite.texture;
            _textureUnitSizeX = texture.width / sprite.pixelsPerUnit;
        }

        private void FixedUpdate()
        {
            Vector3 deltaMovement = cameraTransform.localPosition - _lastCameraPosition;
            transform.localPosition += new Vector3(deltaMovement.x * parallaxEffectMultiplier.x, deltaMovement.y * parallaxEffectMultiplier.y);
            //transform.localPosition = cameraTransform.localPosition;
            _lastCameraPosition = cameraTransform.localPosition;
            
            if (Mathf.Abs(cameraTransform.position.x - transform.position.x) >= _textureUnitSizeX)
            {
                float offsetPositionX = (cameraTransform.position.x - transform.position.x) % _textureUnitSizeX;
                transform.position = new Vector3(cameraTransform.position.x + offsetPositionX, transform.position.y);
            }
        }
    }
}