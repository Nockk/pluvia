﻿using System.Collections;
using UnityEngine;

namespace MiniGames.SheepNightmare
{
    public class SheepNightmare : MonoBehaviour
    {
        private float timeLength = 10.0f;

        private void Start()
        {
            StartCoroutine(StartGameCoroutine());
        }

        private IEnumerator StartGameCoroutine()
        {
            yield return new WaitForSeconds(timeLength);
            
            GameDirector.Instance.EndGame();

            yield return null;
        }
    }
}