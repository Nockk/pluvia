﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;
using Random = UnityEngine.Random;

public class MovieMinigame : MonoBehaviour
{
    [Serializable]
    struct Answer
    {
        public string AnswerText;
        public bool IsCorrect;
    }
    
    [Serializable]
    struct Movies
    {
        public Sprite MovieImage;
        [TextArea(7, 15)] public string MovieQuote;
        public Answer[] Answers;
    }

    // Splash screen stuff
    [SerializeField] private GameObject splashScreen;
    
    // Lobby screen stuff
    [SerializeField] private GameObject lobbyScreen;
    [SerializeField] private GameObject readyButton;
    [SerializeField] private GameObject thumbsUpSar;
    [SerializeField] private GameObject thumbsUpLeo;
    [SerializeField] private GameObject thumbsUpDan;
    [SerializeField] private GameObject thumbsUpRic;
    [SerializeField] private TextMeshProUGUI playerText;
    [SerializeField] private TextMeshProUGUI loadingText;
    
    // Game screen stuff
    [SerializeField] private GameObject gameScreen;
    [SerializeField] private TextMeshProUGUI movieQuote;
    [SerializeField] private Image movieImage;
    [SerializeField] private Image barFill;
    [SerializeField] private Image[] answerImages;
    [SerializeField] private TextMeshProUGUI[] answerTexts;
    [SerializeField] private Color normalColor = new Color(54, 0.4117647f, 0.6705883f);
    [SerializeField] private GameObject[] answer1Icons;
    [SerializeField] private GameObject[] answer2Icons;
    [SerializeField] private GameObject[] answer3Icons;
    [SerializeField] private GameObject[] answer4Icons;
    [SerializeField] private Movies[] movies;
    
    // Scoreboard screen stuff
    [SerializeField] private GameObject scoreboardScreen;
    [SerializeField] private Image sarScore;
    [SerializeField] private Image leoScore;
    [SerializeField] private Image danScore;
    [SerializeField] private Image ricScore;
    

    private bool splashCoroutineEnded = false;
    private bool verifyingAnswers = false;
    private int moviesIndex = 0;
    private int correctAnswer = 0;
    private int? playerChoice = null;
    private int nAnswers1, nAnswers2, nAnswers3, nAnswers4;
    private int[] pointsBots = new int[2];
    private int playerPoints;
    private int maxPointsAnswer = 5;

    // Start is called before the first frame update
    void Start()
    {
        playerText.text = String.Empty;
        loadingText.text = String.Empty;
        readyButton.GetComponent<Button>().interactable = false;
        StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        if (splashCoroutineEnded)
        {
            splashCoroutineEnded = false;
            StartCoroutine(LobbyCoroutine());
        }
        
        // all players ready?
        if (thumbsUpDan.activeSelf && thumbsUpLeo.activeSelf && thumbsUpRic.activeSelf && thumbsUpSar.activeSelf)
        {
            thumbsUpDan.SetActive(false);
            thumbsUpLeo.SetActive(false);
            thumbsUpRic.SetActive(false);
            thumbsUpSar.SetActive(false);
            StartCoroutine(LobbyToGame());
        }

        if (barFill.IsActive() && !verifyingAnswers)
            barFill.fillAmount -= Time.deltaTime / 10;

        if (barFill.fillAmount <= 0)
        {
            barFill.fillAmount = 1;
            StartCoroutine(VerifyAnswers());
        }
    }

    public void StartGame()
    {
        StartCoroutine(SplashToLobby());
    }

    public void ReadyButton()
    {
        thumbsUpLeo.SetActive(true);
    }
    
    private void StartMovieQuiz()
    {
        lobbyScreen.SetActive(false);
        
        movieQuote.text = movies[0].MovieQuote;
        movieImage.sprite = movies[0].MovieImage;
        for (int i = 0; i < movies[0].Answers.Length; i++)
        {
            answerTexts[i].text = movies[0].Answers[i].AnswerText;
            if (movies[0].Answers[i].IsCorrect)
                correctAnswer = i;
        }
        
        gameScreen.SetActive(true);
    }

    private void UpdateQuiz()
    {
        // reset answers selected
        nAnswers1 = 0;
        nAnswers2 = 0;
        nAnswers3 = 0;
        nAnswers4 = 0;
        
        moviesIndex++;
        if (moviesIndex < movies.Length)
        {
            movieQuote.text = movies[moviesIndex].MovieQuote;
            movieImage.sprite = movies[moviesIndex].MovieImage;
            for (int i = 0; i < movies[moviesIndex].Answers.Length; i++)
            {
                answerTexts[i].text = movies[moviesIndex].Answers[i].AnswerText;
                if (movies[moviesIndex].Answers[i].IsCorrect)
                    correctAnswer = i;
            }
        }
        else
            EndGameScreen();
    }

    public void AnswerSelected(int number)
    {
        playerChoice = number;
        for (int i = 0; i < answerImages.Length; i++)
        {
            answerImages[i].GetComponent<Button>().interactable = false;
        }
    }

    IEnumerator VerifyAnswers()
    {
        verifyingAnswers = true;
        GetPlayerChoices();
        
        if (playerChoice != null)
        {
            if (playerChoice.Value == correctAnswer)
            {
                answerImages[playerChoice.Value].color = Color.green;
                playerPoints += 4;
            }
            else
            {
                answerImages[playerChoice.Value].color = Color.red;
                answerImages[correctAnswer].color = Color.green;
            }
        }
        else
        {
            answerImages[correctAnswer].color = Color.green;
        }

        MakeIconsAppear();
        yield return new WaitForSeconds(2);
        MakeIconsDisappear();

        for (int i = 0; i < answerImages.Length; i++)
        {
            answerImages[i].color = normalColor;
            answerImages[i].GetComponent<Button>().interactable = true;
        }
        
        UpdateQuiz();
        verifyingAnswers = false;
    }

    private void GetPlayerChoices() // this is shit code ngl
    {
        // randomize 2 answers
        for (int i = 0; i < 2; i++)
        {
            int choice = Random.Range((int) 0, (int) 4);
            if (choice == correctAnswer)
                pointsBots[i] += 3;
            switch (choice)
            {
                case 0:
                    nAnswers1++;
                    break;
                
                case 1:
                    nAnswers2++;
                    break;
                
                case 2:
                    nAnswers3++;
                    break;
                
                case 3:
                    nAnswers4++;
                    break;
            }
        }

        // one guy is always correct
        switch (correctAnswer)
        {
            case 0:
                nAnswers1++;
                break;
            
            case 1:
                nAnswers2++;
                break;
            
            case 2:
                nAnswers3++;
                break;
            
            case 3:
                nAnswers4++;
                break;
        }

        // player choice
        if (playerChoice != null)
        {
            switch (playerChoice)
            {
                case 0:
                    nAnswers1++;
                    break;
            
                case 1:
                    nAnswers2++;
                    break;
            
                case 2:
                    nAnswers3++;
                    break;
            
                case 3:
                    nAnswers4++;
                    break;
            }
        }
    }

    private void MakeIconsAppear()
    {
        // icons answer 1
        for (int i = 0; i < nAnswers1; i++)
        {
            answer1Icons[i].SetActive(true);
        }
        
        // icons answer 2
        for (int i = 0; i < nAnswers2; i++)
        {
            answer2Icons[i].SetActive(true);
        }
        
        // icons answer 3
        for (int i = 0; i < nAnswers3; i++)
        {
            answer3Icons[i].SetActive(true);
        }
        
        // icons answer 4
        for (int i = 0; i < nAnswers4; i++)
        {
            answer4Icons[i].SetActive(true);
        }
    }
    
    private void MakeIconsDisappear()
    {
        // icons answer 1
        for (int i = 0; i < nAnswers1; i++)
        {
            answer1Icons[i].SetActive(false);
        }
        
        // icons answer 2
        for (int i = 0; i < nAnswers2; i++)
        {
            answer2Icons[i].SetActive(false);
        }
        
        // icons answer 3
        for (int i = 0; i < nAnswers3; i++)
        {
            answer3Icons[i].SetActive(false);
        }
        
        // icons answer 4
        for (int i = 0; i < nAnswers4; i++)
        {
            answer4Icons[i].SetActive(false);
        }
    }
    
    private void EndGameScreen()
    {
        StartCoroutine(GameToEnd());
    }
    
    IEnumerator SplashToLobby()
    {
        splashScreen.SetActive(true);
        yield return new WaitForSeconds(2);
        splashScreen.SetActive(false);
        lobbyScreen.SetActive(true);
        splashCoroutineEnded = true;
    }

    IEnumerator LobbyCoroutine()
    {
        yield return new WaitForSeconds(1);
        playerText.text = "Entering...";
        yield return new WaitForSeconds(1);
        playerText.text = "RIC";
        readyButton.GetComponent<Button>().interactable = true;
        yield return new WaitForSeconds(0.5f);
        thumbsUpSar.SetActive(true);
        thumbsUpDan.SetActive(true);
        yield return new WaitForSeconds(1);
        thumbsUpRic.SetActive(true);
    }

    IEnumerator LobbyToGame()
    {
        loadingText.text = "Loading game...";
        yield return new WaitForSeconds(2);
        StartMovieQuiz();
    }

    IEnumerator GameToEnd()
    {
        sarScore.fillAmount = (float)pointsBots[0] / (maxPointsAnswer * movies.Length);
        danScore.fillAmount = (float)pointsBots[1] / (maxPointsAnswer * movies.Length);
        leoScore.fillAmount = (float)playerPoints / (maxPointsAnswer * movies.Length);
        ricScore.fillAmount = 1;
        
        gameScreen.SetActive(false);
        scoreboardScreen.SetActive(true);
        yield return new WaitForSeconds(3);
        scoreboardScreen.SetActive(false);
    }
}
