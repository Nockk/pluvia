﻿using Episodes;
using Player;
using UI.ParticleSystem;
using UnityEngine;
using UnityEngine.UI;

namespace MiniGames
{
    public class FishGame : MonoBehaviour, ICollision
    {
        [SerializeField] private GameObject fishGame;
        [SerializeField] private Sprite handWFood;
        [SerializeField] private Sprite handSprite;
        [SerializeField] private GameObject fishFood;
        [SerializeField] private GameObject hand;
        [SerializeField] private float rotation = 1f;
        [SerializeField] private EpisodeTask episodeTask;
        [SerializeField] private ParticleManager particleManager;
    
        private int _food = 0;
        private PlayerPActions _playerInputs;
        private Vector2 _mousePos;
        private bool _holding;
        private bool _isAboveFish;
        private float _z;
        private Transform _handTransform;
    
        // Start is called before the first frame update
        void Start()
        {
            _handTransform = hand.GetComponent<Transform>();
            _playerInputs = PlayerInputs.PlayerControls;
            _playerInputs.Player.Enable();
            _playerInputs.UI.Enable();
        }

        // Update is called once per frame
        void Update()
        {
            _mousePos = _playerInputs.UI.Point.ReadValue<Vector2>();
            hand.transform.position = new Vector3(_mousePos.x, _mousePos.y, 0);
        
            RotateHand();
        
            if (_holding && _handTransform.eulerAngles.z > 80f && _handTransform.eulerAngles.z <= 90f)
            {
                particleManager.SetSpawnPosition(_handTransform.position);
                particleManager.StartSpawning();
                _food += 5;
                Debug.Log("throwing food");
            }
        }

        public void RunInteraction()
        {
            PlayerMovementController.SetMoveActive(false);
            hand.GetComponent<Image>().sprite = handSprite;
            fishGame.SetActive(true);
            fishFood.SetActive(true);
            _holding = false;
        }

        // soon deleted
        public void StartGame()
        {
            PlayerMovementController.SetMoveActive(false);
            hand.GetComponent<Image>().sprite = handSprite;
            fishGame.SetActive(true);
            fishFood.SetActive(true);
            _holding = false;
            Cursor.visible = false;
        }

        public void EndGame()
        {
            this.enabled = false;
            PlayerMovementController.SetMoveActive(true);
            fishGame.SetActive(false);
            Cursor.visible = true;
            episodeTask.SetCompleted();
        }

        public void ChangeCursorImage()
        {
            hand.GetComponent<Image>().sprite = handWFood;
            fishFood.SetActive(false);
            _holding = true;
        }

        private void RotateHand()
        {
            float scroll = _playerInputs.Player.RotateHand.ReadValue<float>();

            _z += scroll * rotation * Time.deltaTime;
            _z = Mathf.Clamp(_z, -10f, 90f);
        
            _handTransform.rotation = Quaternion.Euler(0,0, _z);
        }

        public void IncreaseFood(int amount)
        {
        
        }

        public void SetIsAboveBool(bool value)
        {
            _isAboveFish = value;
        }
        
    }
}
