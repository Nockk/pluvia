﻿using System.Collections;
using System.Collections.Generic;
using Episodes;
using Player;
using UI.ParticleSystem;
using UnityEngine;
using UnityEngine.UI;

public class BreakfastGame : MonoBehaviour, ICollision
{
    [SerializeField] private GameObject breakfastGame;
    [SerializeField] private Sprite handWToast;
    [SerializeField] private Sprite handWBread;
    [SerializeField] private Sprite handWKnife;
    [SerializeField] private Sprite handWCoffee;
    [SerializeField] private Sprite handSprite;
    [SerializeField] private GameObject plateBread;
    [SerializeField] private GameObject plateToast;
    [SerializeField] private GameObject plateToastButter;
    [SerializeField] private GameObject toasterBread;
    [SerializeField] private GameObject toasterToast;
    [SerializeField] private GameObject butterKnife;
    [SerializeField] private GameObject coffeeMaker;
    [SerializeField] private GameObject coffeeMakerFire;
    [SerializeField] private GameObject coffeeMakerPutBack;
    [SerializeField] private GameObject hand;
    [SerializeField] private float rotation = 1f;
    [SerializeField] private ParticleManager particleManager;
    [SerializeField] private EpisodeTask episodeTask;

    private float _coffee = 0;
    private PlayerPActions _playerInputs;
    private Vector2 _mousePos;
    //private bool _holding;
    private bool _isAboveCup;
    private bool _finishedCoffee = false;
    private bool _finishedToast = false;
    private float _z;
    private Transform _handTransform;
    private HandState _handState;

    enum HandState
    {
        HoldingCoffee,
        HoldingBread,
        HoldingToast,
        HoldingKnife,
        Free
    }

    // Start is called before the first frame update
    void Start()
    {
        _handTransform = hand.GetComponent<Transform>();
        _playerInputs = PlayerInputs.PlayerControls;
        _playerInputs.Player.Enable();
        _playerInputs.UI.Enable();
        _handState = HandState.Free;
    }

    // Update is called once per frame
    void Update()
    {
        _mousePos = _playerInputs.UI.Point.ReadValue<Vector2>();
        hand.transform.position = new Vector3(_mousePos.x, _mousePos.y, 0);
    
        RotateHand();
    
        //if(_isAboveCup) Debug.Log("is above");
        
        if (_isAboveCup && _handState == HandState.HoldingCoffee && _handTransform.eulerAngles.z > 80f && _handTransform.eulerAngles.z <= 90f)
        {
            particleManager.SetSpawnPosition(_handTransform.position);
            particleManager.StartSpawning();
            
            _coffee += 5 * Time.deltaTime;
            Debug.Log("coffee filing up");
        }

        if (plateToastButter.activeSelf && _coffee >= 20)
        {
            Debug.Log("breakfast completed");
            EndGame();
        }
    }

    public void RunInteraction()
    {
        PlayerMovementController.SetMoveActive(false);
        hand.GetComponent<Image>().sprite = handSprite;
        breakfastGame.SetActive(true);
        _handState = HandState.Free;
    }

    // soon deleted
    public void StartGame()
    {
        PlayerMovementController.SetMoveActive(false);
        hand.GetComponent<Image>().sprite = handSprite;
        breakfastGame.SetActive(true);
        _handState = HandState.Free;
        Cursor.visible = false;
    }

    public void EndGame()
    {
        PlayerMovementController.SetMoveActive(true);
        breakfastGame.SetActive(false);
        Cursor.visible = true;
        episodeTask.SetCompleted();
    }

    private void PickUpCoffee()
    {
        //_finishedCoffee = false;
        _handState = HandState.HoldingCoffee;
        hand.GetComponent<Image>().sprite = handWCoffee;
        coffeeMaker.SetActive(false);
        coffeeMakerPutBack.SetActive(true);
        Debug.Log("Picking up coffee");
    }

    public void ChangeCursorCoffee()
    {
        if (_finishedCoffee && _handState == HandState.Free)
        {
            Debug.Log("entered if");
            PickUpCoffee();
        }
        else if(!_finishedCoffee)
        {
            StartCoroutine(MakingCoffee());
        }
        //put the coffee maker back
        else if (_handState == HandState.HoldingCoffee)
        {
            coffeeMakerPutBack.SetActive(false);
            coffeeMaker.SetActive(true);
            hand.GetComponent<Image>().sprite = handSprite;
            _handState = HandState.Free;
        }
    }
    
    public void ChangeCursorToaster()
    {
        if (_finishedToast && _handState == HandState.Free)
        {
            hand.GetComponent<Image>().sprite = handWToast;
            toasterToast.SetActive(false);
            _finishedToast = false;
            _handState = HandState.HoldingToast;
        }
        else if (!_finishedToast && _handState == HandState.HoldingBread)
        {
            StartCoroutine(MakingToast());
        }
    }

    public void ChangeCursorPlate()
    {
        switch (_handState)
        {
            case HandState.Free:
                Debug.Log("clicked plate and hand is free");
                if (plateBread.activeSelf)
                {
                    hand.GetComponent<Image>().sprite = handWBread;
                    _handState = HandState.HoldingBread;
                    plateBread.SetActive(false);
                }
                break;
            
            case HandState.HoldingToast:
                plateToast.SetActive(true);
                hand.GetComponent<Image>().sprite = handSprite;
                _handState = HandState.Free;
                break;
            
            case HandState.HoldingKnife:
                if (plateToast.activeSelf)
                {
                    plateToastButter.SetActive(true);
                    hand.GetComponent<Image>().sprite = handSprite;
                    _handState = HandState.Free;
                    butterKnife.SetActive(true);
                }
                break;
        }
    }

    public void ChangeCursorButter()
    {
        if (_handState == HandState.Free && plateToast.activeSelf)
        {
            hand.GetComponent<Image>().sprite = handWKnife;
            _handState = HandState.HoldingKnife;
            butterKnife.SetActive(false);
        }
    }

    IEnumerator MakingCoffee()
    {
        Debug.Log("Making coffee");
        coffeeMakerFire.SetActive(true);
        yield return new WaitForSeconds(5);
        coffeeMakerFire.SetActive(false);
        _finishedCoffee = true;
    }
    
    IEnumerator MakingToast()
    {
        Debug.Log("Making toast");
        toasterBread.SetActive(true);
        hand.GetComponent<Image>().sprite = handSprite;
        _handState = HandState.Free;
        yield return new WaitForSeconds(5);
        toasterBread.SetActive(false);
        toasterToast.SetActive(true);
        _finishedToast = true;
    }

    private void RotateHand()
    {
        float scroll = _playerInputs.Player.RotateHand.ReadValue<float>();

        _z += scroll * rotation * Time.deltaTime;
        _z = Mathf.Clamp(_z, -10f, 90f);
    
        _handTransform.rotation = Quaternion.Euler(0,0, _z);
    }

    public void IncreaseFood(int amount)
    {
        
    }

    public void SetIsAboveBool(bool value)
    {
        _isAboveCup = value;
    }
}
