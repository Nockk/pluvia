﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace MiniGames.UltimateFireQuest
{
    public class UFQKnight : UFQBaseEntity
    {
        [SerializeField] 
        private int CurrentPowerPoints;
        [SerializeField] 
        private int PowerPoints;

        [SerializeField]
        private Image powerPointsBar; 
        
        [Header("HyperSlash")]
        [SerializeField] 
        private int hSlashPerHitDamage = 10000;
        [SerializeField, Min(0.0f)] 
        private float attackTime = 3.0f;
        [SerializeField, Min(0.0f)]
        private float splitTime = 1f;
        
        private bool _isHyperAttacking; 
        
        [SerializeField] 
        public byte HyperUsages = 10;
        public byte HyperUsagesLeft { get; private set; }
        
        [Header("Impact Stunner")]
        [SerializeField] 
        public byte ImpactStunUsages = 5;
        public byte ImpactStunUsagesLeft { get; private set; }

        [Header("Three Point Stab")] 
        [SerializeField] 
        private int tPointStabMinDamage = 90000;
        [SerializeField] 
        private int tPointStabMaxDamage = 100000;

        [SerializeField] 
        public byte ThreePointUsages = 10;
        public byte ThreePointUsagesLeft { get; private set; }
        
        [Header("Phoenix Rising")] 
        [SerializeField] 
        private int pRisingMinDamage = 290000;
        [SerializeField] 
        private int pRisingMaxDamage = 299999;
        
        [SerializeField] 
        public byte PhoenixRisingUsages = 5;
        public byte PhoenixRisingUsagesLeft { get; private set; }

        [Header("Block")] 
        [SerializeField]
        public byte BlockUsages = 5;
        public byte BlockUsagesLeft { get; private set; }
        
        [Header("Parry")] 
        [SerializeField]
        public byte ParryUsages = 15;
        public byte ParryUsagesLeft { get; private set; }
        private bool _isParryActive;


        [SerializeField] 
        private TextMeshProUGUI txtHyperSlashUsages;
        [SerializeField] 
        private TextMeshProUGUI txtImpactStunUsages;
        [SerializeField] 
        private TextMeshProUGUI txtThreePointStabUsages;
        [SerializeField] 
        private TextMeshProUGUI txtPhoenixRisingUsages;
        [SerializeField] 
        private TextMeshProUGUI txtBlockUsages;
        [SerializeField] 
        private TextMeshProUGUI txtParryUsages;
        
        [SerializeField] 
        private SpriteRenderer spriteRenderer;
        [SerializeField] 
        private GameObject actionsUI;
        [SerializeField] 
        private GameObject canvasHealthBar;

        public bool IsBlockActive { get; private set; }

        public override void Start()
        {
            base.Start(); 
            HyperUsagesLeft = HyperUsages;
            ImpactStunUsagesLeft = ImpactStunUsages;
            ThreePointUsagesLeft = ThreePointUsages;
            PhoenixRisingUsagesLeft = PhoenixRisingUsages;
            BlockUsagesLeft = BlockUsages;
            ParryUsagesLeft = ParryUsages;
            
            UpdateUI();
            OnAction += UpdateUI;
        }

        private void UpdateUI()
        {
            txtHyperSlashUsages.SetText($"{HyperUsagesLeft}/{HyperUsages}");
            txtImpactStunUsages.SetText($"{ImpactStunUsagesLeft}/{ImpactStunUsages}");
            txtThreePointStabUsages.SetText($"{ThreePointUsagesLeft}/{ThreePointUsages}");
            txtPhoenixRisingUsages.SetText($"{PhoenixRisingUsagesLeft}/{PhoenixRisingUsages}");
            txtBlockUsages.SetText($"{BlockUsagesLeft}/{BlockUsages}");
            txtParryUsages.SetText($"{ParryUsagesLeft}/{ParryUsages}");
        }

        public void AttackHyperSlash()
        {
            if (HyperUsagesLeft <= 0)
                return;
            if (!_isHyperAttacking)
                StartCoroutine(HyperSlashCoroutine());
        }

        private IEnumerator HyperSlashCoroutine()
        {
            _isHyperAttacking = true;
            for (float currentTime = 0.0f; currentTime < attackTime; currentTime += splitTime)
            {
                Debug.Log($"CurrentTime: {currentTime}");
                enemyToAttack.TakeDamage(hSlashPerHitDamage);
                yield return new WaitForSeconds(splitTime);
            }

            HyperUsagesLeft--;
            OnAction.Invoke();
            _isHyperAttacking = false;
        }

        public void AttackImpactStunner()
        {
            if (ImpactStunUsagesLeft <= 0)
                return;
            
            UFQEnemy enemy = (UFQEnemy) enemyToAttack;
            enemy.Stun();

            ImpactStunUsagesLeft--;
            OnAction.Invoke();
        }
        
        public void AttackThreePointStab()
        {
            if (ThreePointUsagesLeft <= 0)
                return;
            
            int damage = Random.Range(tPointStabMinDamage, tPointStabMaxDamage);
            enemyToAttack.TakeDamage(damage);

            ThreePointUsagesLeft--;
            OnAction.Invoke();
        }
        
        public void AttackPhoenixRising()
        {
            if (PhoenixRisingUsagesLeft <= 0)
                return;
            
            int damage = Random.Range(pRisingMinDamage, pRisingMaxDamage);
            enemyToAttack.TakeDamage(damage);
            
            int damagePenalty = damage / 1000;
            TakeDamage(damagePenalty);

            PhoenixRisingUsagesLeft--;
            OnAction.Invoke();
        }

        public void DefendBlock()
        {
            if (BlockUsagesLeft <= 0)
                return;
            
            float result = Random.Range(0f, 1f);
            IsBlockActive = (result > 0.0f && result <= 0.8f);

            BlockUsagesLeft--;
            OnAction.Invoke();

        }

        public void DefendParry()
        {
            if (ParryUsagesLeft <= 0)
                return;

            _isParryActive = true;
            
            ParryUsagesLeft--;
            OnAction.Invoke();
        }

        public override void TakeDamage(int damage, UFQBaseEntity attacker = null)
        {
            if (IsBlockActive)
            {
                StartCoroutine(InfoCoroutine("BLOCKED", Color.yellow));
                IsBlockActive = false;
                return;
            }

            if (_isParryActive)
            {
                if (attacker == null)
                    return;
                attacker.TakeDamage(damage);
                _isParryActive = false;
                return;
            }
            if (!IsAlive)
            {
                spriteRenderer.enabled = false;
                actionsUI.SetActive(false);
                canvasHealthBar.SetActive(false);
                OnDie.Invoke();
                return;
            }
            base.TakeDamage(damage);
        }

        public void SetActionsActive(bool value) => actionsUI.SetActive(value);

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("CaveEntry"))
            {
                UFQUIController.Instance.ResumeDialogue();
            }
        }
    }
}