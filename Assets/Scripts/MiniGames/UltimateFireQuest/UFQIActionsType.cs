﻿using System;

namespace MiniGames.UltimateFireQuest
{
    public interface UFQIActionsType
    {
        void AttackAction(UFQBaseEntity target, Action action);
        void DefenseAction();
    }
}