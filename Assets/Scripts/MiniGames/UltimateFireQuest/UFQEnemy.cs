﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MiniGames.UltimateFireQuest
{
    public class UFQEnemy : UFQBaseEntity
    {
        [SerializeField] 
        public int minDamage = 800;
        [SerializeField] 
        public int maxDamage = 1000;

        [SerializeField]
        public int minDamageToFinish = 300000;
        public int maxDamageToFinish = 450000;
        
        public void Attack()
        {
            if (IsStunned)
            {
                IsStunned = false;
            }
            else
            {
                int damageToFinish = Random.Range(minDamageToFinish, maxDamageToFinish);
                if (CurrentHealth <= damageToFinish)
                {
                    enemyToAttack.TakeDamage(enemyToAttack.CurrentHealth);
                }
                
                int damage = Random.Range(minDamage, maxDamage);
                enemyToAttack.TakeDamage(damage, this);
            }
            OnAction.Invoke();
        }

        public void Stun()
        {
            IsStunned = true;
            StartCoroutine(InfoCoroutine("STUNNED", Color.white));
        }
    }
}