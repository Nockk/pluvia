﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace MiniGames.UltimateFireQuest
{
    public enum UFQEAttack
    {
        HYPER_SLASH,
        IMPACT_STUNNER
    }
    public class UltimateFireQuest : MonoBehaviour
    {
        // UI
        [Header("Loading Screen")]
        [SerializeField, Min(0.0f)] 
        private float loadingTime = 3.0f;
        [SerializeField, Min(0.0f)]
        private float splitTime = 0.1f;
        
        [SerializeField] 
        private Image image;
        
        [SerializeField] 
        private float timeToStart = 1.0f;
        
        // Entities
        [Header("Entities")]
        [SerializeField]
        private UFQKnight knight;
        [SerializeField]
        private UFQEnemy enemy;
        private UFQBaseEntity _entityTurn;

        public bool IsLoaded { get; set; }
        
        void Start()
        {
            StartCoroutine(LoadingScreenCoroutine());
            _entityTurn = knight;
            knight.OnAction += OnKnightAction;
            knight.OnDie += OnKnightDie;
            enemy.OnAction += OnEnemyAction;
        }

        private IEnumerator LoadingScreenCoroutine()
        {
            for (float currentTime = 0.0f; currentTime < loadingTime; currentTime += splitTime)
            {
                Debug.Log($"CurrentTime: {currentTime}");
                yield return new WaitForSeconds(splitTime);
                image.fillAmount = currentTime / loadingTime;
            }
            
            IsLoaded = true;
            Debug.Log($"IsLoaded: {IsLoaded}");
        }

        private IEnumerator ChangeTurn()
        {
            yield return new WaitForSeconds(timeToStart);
            if (_entityTurn == enemy)
            {
                _entityTurn = knight;
                knight.SetActionsActive(true);
            }
            else
            {
                _entityTurn = enemy;
                knight.SetActionsActive(false);
                enemy.Attack();
            }
        }
        
        private void OnKnightAction()
        {
            StartCoroutine(ChangeTurn());
        }
        
        private void OnEnemyAction()
        {
            if (knight.IsAlive)
                StartCoroutine(ChangeTurn());
        }
        
        private void OnKnightDie()
        {
            GameDirector.Instance.ChangeScene(gameObject);
            
        }
    }
}