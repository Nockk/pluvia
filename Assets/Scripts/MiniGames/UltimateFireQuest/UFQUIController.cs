﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using JetBrains.Annotations;
using MiniGames.UltimateFireQuest;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using Utils;

public class UFQUIController : Singleton<UFQUIController>
{
    [Serializable]
    struct Dialogue
    {
        public string Name;
        [TextArea(7, 15)] public string Text;
        [CanBeNull] public Transform playerPosition;
        [CanBeNull] public Transform Camera;
        public PolygonCollider2D CameraConfiner;
        public bool canContinue;
    }

    [SerializeField] private GameObject _loadingScreen;
    [SerializeField] private GameObject _outsideCave;
    [SerializeField] private GameObject _insideCave;
    [SerializeField] private GameObject _dragon;
    [SerializeField] private GameObject _dragonStats;
    [SerializeField] private GameObject _dragonDialogue;
    [SerializeField] private TextMeshProUGUI _dragonDialogueText;
    [SerializeField] private GameObject _player;
    [SerializeField] private GameObject _playerStats;
    [SerializeField] private GameObject _playerDialogue;
    [SerializeField] private TextMeshProUGUI _playerDialogueText;
    [SerializeField] private GameObject _actionsMenu;
    [SerializeField] private Camera _camera;
    [SerializeField] private CinemachineConfiner cameraConfiner;
    [SerializeField, Tooltip("In which dialogue the dragon appears")] private int _dragonAppearence;
    [SerializeField] private Dialogue[] _dialoguesBeforeBattle;
    [SerializeField] private Dialogue[] _dialoguesAfterBattle;

    private UltimateFireQuest _ultimateFireQuest;
    private Transform _cameraTransform;
    private bool _coroutineEnded = false;
    private int _dialogueIndex = 0;
    private UniversalAdditionalCameraData _cameraData;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
        if (_cameraData == null)
            _cameraData = Camera.main.GetUniversalAdditionalCameraData();
        _ultimateFireQuest = gameObject.GetComponent<UltimateFireQuest>();
        _cameraTransform = _camera.transform;
    }

    // Update is called once per frame
    void Update()
    {
        //TEMP
        if (_ultimateFireQuest.IsLoaded)
        {
            StartDialogueSystem();
        }

        if (_coroutineEnded)
        {
            //continue dialogue stuff
            ResumeDialogue();
            _coroutineEnded = false;
        }
    }

    /*
    public void EndPlayerDialogue()
    {
        _outsideCave.SetActive(false);
        _playerDialogue.SetActive(false);
        _insideCave.SetActive(true);
        _dragon.SetActive(true);
        //StartCoroutine(CameraLerpFromTo(cameraStartPos, cameraEndPos, 2f));
    }

    public void EndDragonDialogue()
    {
        _dragonDialogue.SetActive(false);
        _playerStats.SetActive(true);
        _dragonStats.SetActive(true);
        _actionsMenu.SetActive(true);
    }
    */
    
    IEnumerator CameraLerpFromTo(Vector3 pos1, Vector3 pos2, float duration)
    {
        for (float t = 0f; t < duration; t += Time.deltaTime)
        {
            _cameraTransform.position = Vector3.Lerp(pos1, pos2, t / duration);
            yield return 0;
        }
        _cameraTransform.position = pos2;
        _coroutineEnded = true;
    }

    private void StartDialogueSystem()
    {
        //_cameraTransform.position = _dialoguesBeforeBattle[_dialogueIndex].Camera.position;
        _cameraData.SetRenderer(2);
        _loadingScreen.SetActive(false);
        _outsideCave.SetActive(true);
        _playerDialogueText.text = _dialoguesBeforeBattle[_dialogueIndex].Text;
        _playerDialogue.SetActive(true);
        _ultimateFireQuest.IsLoaded = false;
    }

    public void ChangeDialogue()
    {
        _dialogueIndex++;

        if (_dialogueIndex < _dialoguesBeforeBattle.Length)
        {
            var dialogue = _dialoguesBeforeBattle[_dialogueIndex];
            // Move camera to other position?
            /*
            if (_dialoguesBeforeBattle[_dialogueIndex].Camera != null)
            {
                StartCoroutine(CameraLerpFromTo(_cameraTransform.position,
                    _dialoguesBeforeBattle[_dialogueIndex].Camera.position, 2)); // this needs to change
                
                // Activate and deactive stuff
                _dragonDialogue.SetActive(false);
                _playerDialogue.SetActive(false);
                _outsideCave.SetActive(false);
                _insideCave.SetActive(true);
                return; //get out of the function
            }*/

            if (!dialogue.canContinue)
            {
                _cameraData.SetRenderer(0);
                _dragonDialogue.SetActive(false);
                _playerDialogue.SetActive(false);
                return;
            }

            if (dialogue.playerPosition != null)
            {
                if (dialogue.CameraConfiner != null)
                {
                    cameraConfiner.m_BoundingShape2D = dialogue.CameraConfiner;
                }
                StartCoroutine(CameraLerpFromTo(_cameraTransform.position,
                    dialogue.Camera.position, 2));
                _player.transform.position = dialogue.playerPosition.position;
            }

            // Change dialogue text depending on character
            switch (dialogue.Name)
            {
                case "DRAGON":
                    _playerDialogue.SetActive(false);
                    _dragonDialogue.SetActive(true);
                    _dragonDialogueText.text = _dialoguesBeforeBattle[_dialogueIndex].Text;
                    break;
                
                case "CAVALEIRO":
                    if(_dialogueIndex + 1 == _dragonAppearence)
                        _dragon.SetActive(true);
                    _dragonDialogue.SetActive(false);
                    _playerDialogue.SetActive(true);
                    _playerDialogueText.text = _dialoguesBeforeBattle[_dialogueIndex].Text;
                    break;
            }
        }
        else
        {
            StartCombat();
        }
    }

    public void ResumeDialogue()
    {
        _cameraData.SetRenderer(2);
        switch (_dialoguesBeforeBattle[_dialogueIndex].Name)
        {
            case "DRAGON":
                //_playerDialogue.SetActive(false);
                _dragonDialogue.SetActive(true);
                _dragonDialogueText.text = _dialoguesBeforeBattle[_dialogueIndex].Text;
                break;
                
            case "CAVALEIRO":
                //_dragonDialogue.SetActive(false);
                _playerDialogue.SetActive(true);
                _playerDialogueText.text = _dialoguesBeforeBattle[_dialogueIndex].Text;
                break;
        }
    }

    private void StartCombat()
    {
        _cameraData.SetRenderer(0);
        _playerDialogue.SetActive(false);
        _dragonDialogue.SetActive(false);
        _playerStats.SetActive(true);
        _dragonStats.SetActive(true);
        _actionsMenu.SetActive(true);
    }
}
