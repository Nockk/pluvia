﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class UFQActionHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField, TextArea] private string description;
    [SerializeField] private TextMeshProUGUI label;

    public void Start()
    {
        label.enabled = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        label.SetText(description);
        label.enabled = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        label.SetText(description);
        label.enabled = false;
    }
}
