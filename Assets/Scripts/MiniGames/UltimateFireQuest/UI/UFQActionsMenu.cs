﻿using UnityEngine;

namespace MiniGames.UltimateFireQuest.UI
{
    public class UFQActionsMenu : MonoBehaviour
    {
        [SerializeField] private GameObject attackMenu;
        [SerializeField] private GameObject defenseMenu;
        [SerializeField] private GameObject itemsMenu;
        [SerializeField] private GameObject escapeMenu;

        public void OnClickAttackMenu()
        {
            attackMenu.SetActive(true);
            defenseMenu.SetActive(false);
            itemsMenu.SetActive(false);
            escapeMenu.SetActive(false);
        }

        public void OnClickDefenseMenu()
        {
            attackMenu.SetActive(false);
            defenseMenu.SetActive(true);
            itemsMenu.SetActive(false);
            escapeMenu.SetActive(false);
        }

        public void OnClickItemsMenu()
        {
            attackMenu.SetActive(false);
            defenseMenu.SetActive(false);
            itemsMenu.SetActive(true);
            escapeMenu.SetActive(false);
        }

        public void OnClickEscapeMenu()
        {
            attackMenu.SetActive(false);
            defenseMenu.SetActive(false);
            itemsMenu.SetActive(false);
            escapeMenu.SetActive(true);
        }
    }
}
