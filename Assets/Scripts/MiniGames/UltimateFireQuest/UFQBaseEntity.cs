﻿using System;
using System.Collections;
using TMPro;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace MiniGames.UltimateFireQuest
{
    public class UFQBaseEntity : MonoBehaviour
    {
        [ReadOnly]
        public int Health;
        public int CurrentHealth;
        public int PotionPoints;
        public bool IsAlive = true;
        public bool IsStunned { get; protected set; }
        public float InfoDamageTime = 0.1f;
        
        public Action OnAction;
        public Action OnDie;
        
        public UFQBaseEntity enemyToAttack;
        public Image healthBar;
        public TextMeshProUGUI txtHealth;
        public TextMeshProUGUI txtInfo;

        public virtual void Start()
        {
            Debug.Log("StartEntity");
            CurrentHealth = Health;
            txtHealth.SetText($"{CurrentHealth} / {Health}");
        }

        public virtual void TakeDamage(int damage, UFQBaseEntity attacker = null)
        {
            Debug.Log($"Taking Damage: {damage}");
            
            IsAlive = (CurrentHealth -= damage) > 0;
            StartCoroutine(InfoCoroutine($"-{damage}"));
            
            txtHealth.SetText($"{CurrentHealth} / {Health}");
            healthBar.fillAmount = (float) CurrentHealth / Health;
        }

        protected IEnumerator InfoCoroutine(string text, Color? color = null)
        {
            txtInfo.enabled = true;

            txtInfo.color = color ?? Color.red;
            txtInfo.SetText(text);
            yield return new WaitForSeconds(InfoDamageTime);
            
            txtInfo.enabled = false;
        }
    }
}