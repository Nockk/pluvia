﻿using System;
using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class Interaction : MonoBehaviour
{
    [SerializeField] private GameObject _image;
    [SerializeField] private UnityEvent _event;
    //[SerializeField] private GameObject _interactable;
    //[SerializeField] private List<GameObject> _interactables;

    private bool _inRange;
    private PlayerPActions _controls;

    void Start()
    {
        
    }

    private void OnEnable()
    {
        _controls = PlayerInputs.PlayerControls;
        _controls.Player.Enable();
        _controls.Player.Interaction.performed += InteractionPerformed;
    }

    // Update is called once per frame
    void Update()
    {
        if (_inRange)
            _image.SetActive(true);
        else
            _image.SetActive(false);
    }

    private void InteractionPerformed(InputAction.CallbackContext obj)
    {
        if (_inRange)
        {
            // soon deleted prob
            Debug.Log("E pressed");
            _event.Invoke();
            
            // code that will replace it
            /*_interactable.GetComponent<IInteractable>().RunInteraction();

            foreach (GameObject interactable in _interactables)
            {
                interactable.GetComponent<IInteractable>().RunInteraction();
            }*/
        }
            
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
            _inRange = true;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
            _inRange = false;
        
    }
}
