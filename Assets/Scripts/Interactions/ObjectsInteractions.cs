﻿using System;
using Interactions.Systems;
using UnityEngine;

namespace Interactions
{
    public class ObjectsInteractions : MonoBehaviour
    {
        [SerializeField] private BathRoomDivision bathRoomDivision;

        public void PrintConsole(string text)
        {
            Debug.Log(text);
        }

        public void SetDoorBathroomActive(bool value)
        {
            bathRoomDivision.SetRoomActive(value);
        }
    }
}