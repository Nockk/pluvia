﻿using System;
using System.Collections;
using System.Collections.Generic;
using MiniGames;
using UnityEngine;

public class DetectCollisionTrigger : MonoBehaviour
{
    [SerializeField] private GameObject game;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Hand"))
        {
            //Debug.Log("trigger enter");
            game.GetComponent<ICollision>().SetIsAboveBool(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Hand"))
        {
            //Debug.Log("trigger exit");
            game.GetComponent<ICollision>().SetIsAboveBool(false);
        }
    }
}
