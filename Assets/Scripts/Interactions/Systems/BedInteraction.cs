﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BedInteraction : MonoBehaviour
{
    [SerializeField]
    private GameObject sleepDream;

    public void Sleep()
    {
        GameDirector gameDirector = GameDirector.Instance;

        int currentEpisode = gameDirector.GetCurrentEpisodeIndex;
        switch (currentEpisode)
        {
            case 1:
                gameDirector.ChangeToScene(sleepDream);
                break;
            default:
                break;
        }
    }
}
