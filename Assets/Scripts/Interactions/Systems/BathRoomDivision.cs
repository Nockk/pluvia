﻿using UnityEngine;

namespace Interactions.Systems
{
    public class BathRoomDivision : MonoBehaviour
    {
        [SerializeField] 
        private GameObject wall;

        [SerializeField] 
        private GameObject wallCollider;
        [SerializeField] 
        private GameObject room;

        [SerializeField] private Transform spawn, exit;

        [SerializeField] private Rigidbody2D player;

        public void SetRoomActive(bool value)
        {
            wall.SetActive(!value);
            wallCollider.SetActive(!value);
            room.SetActive(value);
            
            player.transform.position = value ? spawn.position : exit.position;    
        }
    }
}