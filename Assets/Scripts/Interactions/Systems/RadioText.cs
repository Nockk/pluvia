﻿using System;
using System.Collections;
using TMPro;
using Unity.Collections;
using UnityEngine;

namespace Interactions.Systems
{
    public class RadioText : MonoBehaviour
    {
        // UI
        [SerializeField] private GameObject canvas;
        [SerializeField] private TextMeshProUGUI textOutput;

        [SerializeField]
        private RadioSpeaks radioSpeaks;

        private RadioOutput[] _radioOutputs; 
        
        private int _speakTextIndex;
        private bool _isPlaying;
        public bool IsActive { get; private set; }
        
        // TODO (bruno): Audio file will be needed here

        private void Start()
        {
            
            VerifyRadioOuputs();
            SetActive(true);
        }

        private void VerifyRadioOuputs()
        {
            if (radioSpeaks == null || radioSpeaks.GetSpeaksList() == null) return;
            _radioOutputs = radioSpeaks.GetSpeaksList();
            int maxCharacters = radioSpeaks.MaxCharacters;
            for (int i = 0; i < _radioOutputs.Length; ++i)
            {
                RadioOutput radioOut = _radioOutputs[i];
                int radioCharacters = radioOut.Text.Length;
                if (radioCharacters > maxCharacters)
                {
                    Debug.LogError($"{radioOut.Text} has {radioCharacters} characters. The maximum is {maxCharacters}");
                }
            }
        }

        private void OnValidate()
        {
            VerifyRadioOuputs();
        }

        public void SetActive(bool value)
        {
            IsActive = value;
            canvas.SetActive(value);

            if (IsActive && !_isPlaying)
            {
                StartCoroutine(Play());    
            }
        }

        private IEnumerator Play()
        {
            _isPlaying = true;
            for (; _speakTextIndex < _radioOutputs.Length; ++_speakTextIndex)
            {
                RadioOutput radioOut = _radioOutputs[_speakTextIndex];
                textOutput.SetText(radioOut.Text);
                yield return new WaitForSeconds(radioOut.Time);
            }

            _isPlaying = false;
            SetActive(false);
        }
    }
}