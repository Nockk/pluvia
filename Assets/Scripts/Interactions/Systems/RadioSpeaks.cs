﻿using System;
using UnityEngine;

namespace Interactions.Systems
{
    [Serializable]
    public struct RadioOutput
    {
        [TextArea(3, 3)] public string Text;
        public int Length;
        public float Time;
    }
    [CreateAssetMenu(fileName = "radioSpeak", menuName = "RadioSpeaks", order = 0)]
    public class RadioSpeaks : ScriptableObject
    {
        [SerializeField] private int maxCharacters = 82;
        
        [SerializeField] 
        private RadioOutput[] speaksList;

        public ref RadioOutput[] GetSpeaksList() => ref speaksList;

        public int MaxCharacters => maxCharacters;
    }
}