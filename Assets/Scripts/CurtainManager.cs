﻿using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class CurtainManager : MonoBehaviour
{
    public GameObject curtain;
    public GameObject[] curtains;
    public float sizingFactor = 0.02f;
    private PlayerPActions _playerInputs;

    void Start()
    {
        _playerInputs = PlayerInputs.PlayerControls;
        _playerInputs.Player.Enable();
        _playerInputs.UI.Enable();
    }

    void Update()
    {
        if (Mouse.current.leftButton.isPressed) {
            for (int i = 0; i < curtains.Length; i++)
            {    
                Vector3 size = curtains[i].transform.localScale;
                size.x = Mouse.current.position.ReadValue().x / 6 * sizingFactor;
                curtains[i].transform.localScale = size;
            } 
        }
    }
}