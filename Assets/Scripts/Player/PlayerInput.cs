﻿namespace Player
{
    public static class PlayerInputs
    {
        private static PlayerPActions _controls;

        public static PlayerPActions PlayerControls
        {
            get
            {
                if (_controls != null) return _controls;
                return _controls = new PlayerPActions();
            }
        }
    }
}