﻿using System;
using UnityEngine;

namespace Player
{
    public class PlayerMovementController : MonoBehaviour
    {
        [SerializeField]
        private float speed = 5f;

        [SerializeField] 
        private Transform body;
        [SerializeField]
        private bool canMoveYaxis = true;

        [SerializeField] 
        private Animator animator;
        
        private float _xScale;
        
        private Rigidbody2D _rigidbody;
        private PlayerPActions _controls;

        private static bool _canMove = true;
        

        public static void SetMoveActive(bool status) => _canMove = status;
        
        private void OnEnable()
        {
            _canMove = true;
            _controls = PlayerInputs.PlayerControls;
            _controls.Player.Move.Enable();
        }
        
        private void OnDisable()
        {
            _controls.Player.Move.Disable();
        }

        void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _xScale = body.localScale.x;
        }

        void FixedUpdate()
        {
            if (!_canMove) 
                return;
            
            Vector2 movementInput = _controls.Player.Move.ReadValue<Vector2>();
            
            Move(movementInput);
            ScaleToDirection(movementInput.x);
        }

        private void ScaleToDirection(float xMovement)
        {
            if (xMovement == 0.0f) 
                return;

            if (xMovement > 0.0f)
            {
                body.localScale = new Vector3(_xScale, body.localScale.y, body.localScale.z);
            }
            else if (xMovement < 0.0f)
            {
                body.localScale = new Vector3(-_xScale, body.localScale.y, body.localScale.z);
            }
        }

        private void Move(Vector2 movementInput)
        {
            Vector3 move = Vector3.zero;
            
            move += transform.right * movementInput.x + (transform.up * movementInput.y * Convert.ToInt16(canMoveYaxis));
            move *= speed * Time.fixedDeltaTime;

            _rigidbody.MovePosition(transform.position + move);
            animator.SetFloat("Speed", move.sqrMagnitude);
        }
    }
}
