﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurtainSlider : MonoBehaviour
{
    public GameObject curtain;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(this.GetComponent<Slider>().value);    
        Vector3 size = curtain.transform.localScale;
        size.x = this.GetComponent<Slider>().value;
        curtain.transform.localScale = Vector3.Lerp(curtain.transform.localScale,size, Time.deltaTime*10);
    }

}
