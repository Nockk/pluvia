﻿using System;
using UnityEngine;
 
[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
 
public class CRTPostProcess : MonoBehaviour 
{
    public Shader shader;
    private Material _material;
    
    [Range(-3, 20)] public float contrast = 0.0f;
    [Range(-200, 200)] public float brightness = 0.0f;
 
    protected Material material
    {
        get
        {
            if (_material == null)
            {
                _material = new Material(shader);
                _material.hideFlags = HideFlags.HideAndDontSave;
            }
            return _material;
        }
    }

    private void Start()
    {
        material.SetFloat("_Contrast", contrast);
        material.SetFloat("_Br", brightness);
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (shader == null) return;
        Material mat = material;
        Graphics.Blit(source, destination, mat);
        Debug.Log("RenderImage");
    }
 
    void OnDisable()
    {
        if (_material)
        {
            DestroyImmediate(_material);
        }
    }
}