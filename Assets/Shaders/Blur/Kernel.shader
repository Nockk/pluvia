Shader "My/Kernel"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Size ("Size", float) = 1.0
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float _Size;

            fixed4 frag (v2f i) : SV_Target
            {
                // Box blur
                /*float3x3 kernel =
                {
                    1, 1, 1,
                    1, 1, 1,
                    1, 1, 1
                };

                kernel /= 9.0f;*/

                // Gaussian blur
                float3x3 kernel =
                {
                    1, 2, 1,
                    2, 4, 2,
                    1, 2, 1
                };
                
                kernel /= 16.0f;

                float2 texelWidth = 1.0f / _ScreenParams.xy * _Size;

                float2 uv = i.uv;
                float changes = 1;
                changes *= pow(16.0 * uv.x * (1.0-uv.x) * uv.y * (1.0-uv.y), 0.4); // adds the black borders
                changes *= 0.7f; // darkens it a bit

                float3 color;
                for (int x = -1; x < 2; x++) {
                    for (int y = -1; y < 2; y++) {
                        float2 offset = float2(x, y) * texelWidth;
                        color += tex2D(_MainTex, uv + offset) * kernel[x+1][y+1];
                    }
                }

                color *= changes;

                return float4(color, 1);
            }
            ENDCG
        }
    }
}
