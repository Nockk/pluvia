using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[ImageEffectAllowedInSceneView]
public class Kernel : MonoBehaviour
{
    public float Size = 1.0f;

    Material kernel;

    void Start()
    {
        kernel = new Material(Shader.Find("My/Kernel"));
    }

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        if (!kernel) {
            Graphics.Blit(src, dest);
            return;
        }

        kernel.SetFloat("_Size", Size);
        Graphics.Blit(src, dest, kernel);
    }
}
