﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/PixelateShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderPipeline" = "UniversalPipeline" }

		ZTest Always Cull Off ZWrite Off Fog { Mode off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#pragma target 3.0

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 screenPosition : TEXCOORD1;
			};

			sampler2D _MainTex;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.screenPosition = ComputeScreenPos(o.vertex);
				return o;
			}
			

			float4 frag (v2f i) : SV_Target
			{	
				float2 pixelScreen = i.screenPosition.xy * _ScreenParams.xy / i.screenPosition.w;
				float2 pixelScaling = 800 * pixelScreen;
   				i.uv = round(i.uv * pixelScaling)/ pixelScaling;
   				return tex2D(_MainTex, i.uv);
			}

			ENDCG
		}
	}
}

