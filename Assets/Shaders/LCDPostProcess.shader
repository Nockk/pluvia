﻿Shader "Custom/LCDPostProcess" 
{
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _VignettePower("Vignette Power", float) = 0.3
    }

    SubShader {
        Pass {

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma target 3.0

            struct v2f 
            {
                float4 pos      : POSITION;
                float2 uv       : TEXCOORD0;
                float4 screenPosition : TEXCOORD1;
            };

            uniform sampler2D _MainTex;
            uniform float _VignettePower;
            uniform float _EffectStrength;
            

            v2f vert(appdata_img v)
            {
                v2f o;
                // Transforma um ponto do espaço do objeto para coordenadas no espaço da câmara
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = MultiplyUV(UNITY_MATRIX_TEXTURE0, v.texcoord);
                // Screen coordinate
                o.screenPosition = ComputeScreenPos(o.pos);
                return o;
            }

            half4 frag(v2f i): COLOR
            {
                half4 color = tex2D(_MainTex, i.uv);
                // Calculate pixel screen coordinate, but in the real pixels.
                // ScreenParams retorna o tamanho do ecrã. i.screenPosition é o tamanho do pixel
                float2 pixelScreen = i.screenPosition.xy * _ScreenParams.xy / i.screenPosition.w;
                
                int pp = (int) pixelScreen.x % 3;
                float4 finalColor = float4(0, 0, 0, 1);
                
                if (pp == 1)
                    finalColor.b = color.b;
                else if  (pp == 2)
                    finalColor.r = color.r;
                else
                    finalColor.g = color.g;

                float2 uv = i.uv;
                float vignette = (uv.x * uv.y * (1 - uv.x) * (1 - uv.y));
                vignette = pow(vignette, _VignettePower);
                finalColor *= vignette;
                
                return finalColor;
            }

            ENDCG
        }
    }
    FallBack "Diffuse"
}