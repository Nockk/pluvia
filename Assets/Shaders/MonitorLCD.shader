﻿Shader "Unlit/MonitorLCD"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Texture", 2D) = "white" {}
        _Pixels ("Pixels", Vector) = (10,10,0,0)
        _LCDTex ("LCD Texture", 2D) = "white" {}
        _LCDPixels ("LCD Pixels", Vector) = (3,3,0,0)
        _DistanceOne ("Distance one", float) = 0.5
        _DistanceZero ("Distance zero", float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _LCDTex;
            float4 _Pixels;
            float4 _LCDPixels;
            float _DistanceOne;
            float _DistanceZero;
            float4 _MainTex_ST;
            fixed4 _Color;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = round(i.uv * _Pixels.xy) / _Pixels.xy;
                fixed4 a = tex2D(_MainTex, uv);

                float2 uvLcd = i.uv * _Pixels.xy / _LCDPixels;
                fixed4 d = tex2D(_LCDTex, uvLcd);

                float dist = distance(_WorldSpaceCameraPos, i.vertex);
                float alpha = saturate
                (
                    (dist - _DistanceOne) / (_DistanceZero - _DistanceOne)
                );
                
                
                return lerp(d * a, a, alpha) * _Color;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
