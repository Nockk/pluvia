﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using TMPro;

public class ClockDigital : MonoBehaviour
{
    public TMP_Text m_TextComponent;

    void Start (){
    m_TextComponent = GetComponent<TMP_Text>();
    }

    void Update (){
        DateTime time = DateTime.Now;
        string hour = LeadingZero( time.Hour );
        string minute = LeadingZero( time.Minute );
        //string second = LeadingZero( time.Second );

        m_TextComponent.text = hour + ":" + minute; //+ ":" + second;
    }

    string LeadingZero (int n){
        return n.ToString().PadLeft(2, '0');
    }
}
