﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class DesktopManager : MonoBehaviour
{
    public GameObject DesktopUI;
    public GameObject CGUI;
    public GameObject TaksUI;
    private UniversalAdditionalCameraData _cameraData;
    private void OnEnable()
    {
        Debug.Log("Starting desktop...");
        if(_cameraData == null)
            _cameraData = Camera.main.GetUniversalAdditionalCameraData();
        _cameraData.SetRenderer(1);
    }

    private void OnDisable()
    {
        //_cameraData.SetRenderer(0);
    }

    public void TurnOffBtn()
    {
        DesktopUI.SetActive(false);
        CGUI.SetActive(false);
        TaksUI.SetActive(true);
        _cameraData.SetRenderer(0);
    }

    public void TurnOnBtn()
    {
        DesktopUI.SetActive(true);
        TaksUI.SetActive(false);
    }

    public void TBBtn(GameObject btn)
    {
        if(btn.active == true){
            btn.SetActive(false);
            btn.GetComponent<ApplicationManager>().isMinimized = true;
        }else
        {
            btn.SetActive(true);
            btn.GetComponent<ApplicationManager>().isMinimized = false;
            btn.GetComponent<ApplicationManager>().isClosed = false;

        }
    }

    public void ABTn(GameObject app)
    {
        app.SetActive(false);
        app.GetComponent<ApplicationManager>().isClosed = true;
    }

    public void MBTn(GameObject app)
    {
        app.SetActive(false);
        app.GetComponent<ApplicationManager>().isMinimized = true;
    }

}
