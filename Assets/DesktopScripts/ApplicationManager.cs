﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class ApplicationManager : MonoBehaviour
{
    public GameObject SaraChat, GroupChat, RicardoChat, DanielChat;

    public bool isMinimized = false;
    public bool isClosed = true;

    public void SaraBtn(){
        SaraChat.SetActive(true);
        GroupChat.SetActive(false);
        RicardoChat.SetActive(false);
        DanielChat.SetActive(false);
    }
    public void RicardoBtn(){
        SaraChat.SetActive(false);
        GroupChat.SetActive(false);
        RicardoChat.SetActive(true);
        DanielChat.SetActive(false);
    }
    public void DanielBtn(){
        SaraChat.SetActive(false);
        GroupChat.SetActive(false);
        RicardoChat.SetActive(false);
        DanielChat.SetActive(true);
    }
    public void GroupBtn(){
        SaraChat.SetActive(false);
        GroupChat.SetActive(true);
        RicardoChat.SetActive(false);
        DanielChat.SetActive(false);
    }
}
