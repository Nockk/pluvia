﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Player;
using UnityEngine.Rendering.Universal;

[ExecuteInEditMode]
public class VirtualOS : MonoBehaviour {

    public bool useTestCodeForTasks;

    public Camera viewCam;
    public VirtualProgram taskSelect;
    public VirtualProgram editor;
    public VirtualProgram console;
    public VirtualProgram instructions;

    public enum Program { None, TaskSelect, Editor, Console, Instructions }
    public Program currentProgram;

    bool needsUpdate;

    static VirtualOS _instance;
    Task currentTask;
    private PlayerPActions _playerInputs;
    public GameObject[] HideGO;

    public Camera HideCamera;
    public Canvas canvas;

    void Awake () {
        SetProgram (currentProgram);
    }

    static VirtualOS Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<VirtualOS> ();
            }
            return _instance;
        }
    }

    public static bool Active {
        get {
            return Instance != null;
        }
    }

    public static void RegisterTask (Task task) {
        Instance.currentTask = task;
        Instance.CopyTargetCamera ();
        Instance.RunTask ();
    }

    void RunTask () {
        string code = ((VirtualScriptEditor) editor).code;
        bool runnable = true;
        if (useTestCodeForTasks) {
            code = currentTask.testCode.text;
            Debug.Log ("Running task with test code");
        }
        for (int i = 0; i < code.Length; i++){
            char c = code[i];
            if (c.ToString() == "_"){
                runnable = false;
            }
        }
        if(runnable) {
            currentTask.StartTask (code);
        }   
        ((VirtualScriptEditor) editor).SetTaskInfo (currentTask.taskInfo);

        SetProgram (Program.Console);
    }

    void StopTask () {
        if (currentTask != null) {
            currentTask.StopTask ();
        }
    }

    void Update () {

        HandleInput ();

        if (needsUpdate && !Application.isPlaying) {
            needsUpdate = false;
            SetProgram (currentProgram);
        }
    }

    void HandleInput () {
        // Open code editor
        if (/*Input.GetKeyDown (KeyCode.E)*/ Keyboard.current.eKey.isPressed && ControlOperatorDown ()) {
            if (currentProgram == Program.Console) {
                StopTask ();
                SetProgram (Program.Editor);
            }
        }

        // Run code
        if (/*Input.GetKeyDown (KeyCode.R)*/ Keyboard.current.rKey.isPressed && ControlOperatorDown ()) {
            if (currentProgram == Program.Editor) {
                RunTask ();
            }
        }

        // Open task menu
        if (/*Input.GetKeyDown (KeyCode.T)*/ Keyboard.current.tKey.isPressed && ControlOperatorDown ()) {
            HideCamera.gameObject.SetActive(true);
            viewCam.gameObject.SetActive(false);
            if (currentProgram != Program.TaskSelect) {
                StopTask ();
                SetProgram (Program.TaskSelect);            
                for (int i = 0; i < HideGO.Length; i++)
                {
                    HideGO[i].SetActive(true);
                }
            }
            canvas.worldCamera = HideCamera;
        }
    }

    public void SetProgram (Program program) {
        currentProgram = program;
        if (taskSelect != null) {
            taskSelect.SetActive (currentProgram == Program.TaskSelect);
            editor.SetActive (currentProgram == Program.Editor);
            console.SetActive (currentProgram == Program.Console);
            instructions.SetActive (currentProgram == Program.Instructions);
        }
    }

    public void CopyTargetCamera () {
        if (FindObjectOfType<CameraTarget> ()) {
            Camera targetCam = FindObjectOfType<CameraTarget> ().GetComponent<Camera> ();
            // Copy settings
            canvas.worldCamera = viewCam;
            viewCam.gameObject.SetActive(true);
            viewCam.transform.position = targetCam.transform.position;
            viewCam.transform.rotation = targetCam.transform.rotation;
            viewCam.orthographic = targetCam.orthographic;
            viewCam.orthographicSize = targetCam.orthographicSize;
            viewCam.fieldOfView = targetCam.fieldOfView;
            // Disable
            targetCam.gameObject.SetActive (false);
        }
    }

    bool ControlOperatorDown () {
        //bool ctrl = Input.GetKey (KeyCode.LeftControl) || Input.GetKey (KeyCode.RightControl);
        //bool cmd = Input.GetKey (KeyCode.LeftCommand) || Input.GetKey (KeyCode.RightCommand);
        bool ctrl = Keyboard.current.ctrlKey.isPressed;
        bool cmd = Keyboard.current.leftCommandKey.isPressed || Keyboard.current.rightCommandKey.isPressed;
        return ctrl || cmd;
    }

    void OnValidate () {
        needsUpdate = true;
    }
}